package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (isValid(start, end)) {
            return generateTable(start, end);
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        boolean tagOne = isInRange(start);
        boolean tagTwo = isInRange(end);
        boolean tagThree = isStartNotBiggerThanEnd(start, end);

        if (tagOne && tagTwo && tagThree) {
            return Boolean.TRUE;
        }
        else {
            return Boolean.FALSE;
        }

    }

    public Boolean isInRange(int number) {
        if (1 <= number && number <= 1000){
            return Boolean.TRUE;
        }
        else {
            return Boolean.FALSE;
        }
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {

        if (start <= end){
            return Boolean.TRUE;
        }
        else {
            return Boolean.FALSE;
        }
    }

    public String generateTable(int start, int end) {
        String tableString = "";
        for (int inter = start; inter <= end; inter++) {

            if (inter==start) {
                tableString = tableString + generateLine(start, inter);
            }
            else {
                tableString = tableString + "\r\n" + generateLine(start, inter);
            }
        }
        return tableString;
    }

    public String generateLine(int start, int row) {
        String lineString = "";
        for (int i = 0; i <= row-start; i++) {
            lineString = lineString + "  " + generateSingleExpression(start + i, row);
        }
        return lineString.trim();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        int output = multiplicand * multiplier;
        return String.format("%d", multiplicand) + '*' + String.format("%d", multiplier) + '=' + String.format("%d", output);
    }
}
